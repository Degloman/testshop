<?php
namespace Model;

class orders_model
{
    const TABLE = 'orders';

    public $id_order;
    public $id_product;
    public $firstName;
    public $lastName;
    public $date;
    public $mail;

    public static function showOrders()
    {
        $db = new Database();
        $data = $db->query('SELECT * FROM ' . self::TABLE, self::class);
        return $data;
    }

    public static function deleteOrder($id)
    {
        $db = new Database();
        $sql = 'DELETE FROM ' . self::TABLE . ' WHERE id_order=' . $id . '';
        $db->delete($sql);
    }

}