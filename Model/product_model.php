<?php
namespace Model;

class product_model
{
    const TABLE = 'products';

    public $id_product;
    public $id_category;
    public $name;
    public $description;
    public $price;
    public $image;

    public static function showProducts($params)
    {
        $db = new Database();
        $data = $db->execute('SELECT * FROM ' . self::TABLE . ' WHERE id_category=:id ', self::class, $params);
        return $data;
    }

    public static function showAllProducts()
    {
        $db = new Database();
        $data = $db->query('SELECT * FROM ' . self::TABLE . '', self::class);
        return $data;
    }

    public static function insertProduct($data = [])
    {
        foreach (get_class_vars(static::class) as $value => $k) {
            $fields[] = $value;
            $ins[] = ':' . $value;
            $params[':' . $value] = '';
        }
        $temp = array_combine($ins, $data);

        $ins = implode(',', $ins);
        $fields = implode(', ', $fields);

        $db = new Database();
        $data = $db->insert("INSERT INTO products ($fields) VALUES ($ins)", $temp);
    }

    public static function deleteByID($id)
    {
        $db = new Database();
        $sql = 'DELETE FROM ' . self::TABLE . ' WHERE id_product=' . $id . '';
        $db->delete($sql);
    }
}