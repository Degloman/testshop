<?php
namespace Model;

class Database
{
    protected $dbh;
    // server settings
    private $host = '127.0.0.1';
    private $dbaseName = 'testShop';
    private $nameUser = 'root';
    private $password = '';
    
    public function __construct()
    {
        $this->dbh = new \PDO('mysql:host=' . $this->host . ';
        dbname=' . $this->dbaseName . '',
            $this->nameUser,
            $this->password);

    }
    
    //ORM function with params
    public function execute($sql, $class, $params = [])
    {
        $exec = $this->dbh->prepare($sql);
        $exec->execute($params);
        return $exec->fetchAll(\PDO::FETCH_CLASS, $class);
    }

    //ORM function without params
    public function query($sql, $class)
    {
        $q = $this->dbh->prepare($sql);
        $q->execute();
        return $q->fetchAll(\PDO::FETCH_CLASS, $class);
    }

    //INSERT
    public function insert($sql, $params = [])
    {
        $insert = $this->dbh->prepare($sql);
        $insert->execute($params);
    }

    //DELETE
    public function delete($sql)
    {
        $this->dbh->exec($sql);
    }
}
