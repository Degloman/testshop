<?php
namespace Model;

class object_model
{
    const TABLE = 'products';

    public $id_product;
    public $id_category;
    public $name;
    public $description;
    public $price;
    public $image;

    public static function showObject($params)
    {
        $db = new Database();
        $data = $db->execute('SELECT * FROM ' . self::TABLE . ' WHERE id_product=:id ', self::class, $params);
        return $data;
    }
}