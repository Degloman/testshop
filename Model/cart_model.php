<?php
namespace Model;

class cart_model
{
    const TABLE = 'orders';

    private $id_order;
    private $id_product;
    private $firstName;
    private $lastName;
    private $date;
    private $mail;

    public function acceptPurchase($data = [])
    {
        foreach (get_class_vars(static::class) as $value => $k) {
            $fields[] = $value;
            $ins[] = ':' . $value;
            $params[':' . $value] = '';

        }
        $temp = array_combine($ins, $data);

        $ins = implode(',', $ins);
        $fields = implode(', ', $fields);

        $db = new Database();
        $db->insert("INSERT INTO orders ($fields) VALUES ($ins)", $temp);
    }
}