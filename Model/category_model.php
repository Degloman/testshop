<?php
namespace Model;

class category_model
{
    const TABLE = 'categories';

    public $id_category;
    public $name;
    
    public static function showCategory()
    {
        $db = new Database();
        $data = $db->query('SELECT * FROM ' . self::TABLE, self::class);
        return $data;
    }

    public static function insertCategory($data = [])
    {
        foreach (get_class_vars(static::class) as $value => $k) {
            $fields[] = $value;
            $ins[] = ':' . $value;
            $params[':' . $value] = '';

        }
        $temp = array_combine($ins, $data);

        $ins = implode(',', $ins);
        $fields = implode(', ', $fields);

        $db = new Database();
        $db->insert('INSERT INTO ' . static::TABLE . ' (' . $fields . ') VALUES (' . $ins . ')', $temp);
    }

    public static function deleteCategory($id)
    {
        $db = new Database();
        $sql = 'DELETE FROM ' . self::TABLE . ' WHERE id_category=' . $id . '';
        $db->delete($sql);
    }
}