<?php

class route
{
    static function start()
    {

        $controller_name = 'category';
        $action_name = 'Index';

        $routes = route::regex();

        // получаем имя контроллера
        if (!empty($routes)) {
            $controller_name = $routes;
        }

        // получаем имя экшена
//        if ( !empty($routes[2]) )
//        {
//            $action_name = $routes[2];
//        }

        // добавляем префиксы
        $model_name = $controller_name . '_model';
        $controller_name = $controller_name . '_controller';

        // подцепляем файл с классом модели (файла модели может и не быть)
        $model_file = strtolower($model_name) . '.php';
        $model_path = 'Model\\' . $model_file;

        if (file_exists($model_path)) {
            include 'Model\\' . $model_file;
        }

        // подцепляем файл с классом контроллера
        $controller_file = strtolower($controller_name) . '.php';
        $controller_path = __DIR__ . '\Controllers\\' . $controller_file;

        if (file_exists($controller_path)) {
            include __DIR__ . '\Controllers\\' . $controller_file;

        } else {
            Route::ErrorPage404();
        }

        // создаем контроллер
        $controller_name = '\Controllers\\' . $controller_name;
        $controller = new $controller_name;


        if (method_exists($controller, 'action' . $action_name)) {
            // вызываем действие контроллера
            $controller->action($action_name);
        } else {
            // здесь также разумнее было бы кинуть исключение
            Route::ErrorPage404();
        }
    }

    function ErrorPage404()
    {

        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }

    static function regex()
    {
        if (preg_match('/[a-zA-Z]+/', $_SERVER['REQUEST_URI'], $arr)) {
            return $arr[0];
        } else return $routes = '';
    }
}