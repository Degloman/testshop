<?php

//spl_autoload_extensions(".php"); // comma-separated list
//spl_autoload_register(
//    function ($class) {
//        $fileName =  __DIR__ . '/'
//            . str_replace('\\', '/', $class) . '.php';
//        if (file_exists($fileName)) {
//            require $fileName;
//        }
//    }
//);

function __autoload($class)
{
    require __DIR__ . '/' . $class . '.php';
}