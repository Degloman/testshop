<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">


    <title><?php echo $object[0]->name ?></title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar.css" rel="stylesheet">

</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../">Главная</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/category">Категории</a></li>
                    <li class="active"><a>Товар</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a>Меню администратора:</a></li>
                    <li><a href="/admCategory">Категории</a></li>
                    <li><a href="/admProduct">Товары</a></li>
                    <li><a href="/admOrders">Заказы</a></li>
                </ul>
                </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h1><?php echo $object[0]->name ?></h1>
        <table class="table">
            <thead>
            <tr>
                <th>Описание</th>
                <th>Изображение</th>
                <th>Цена</th>
                <th></th>
            </tr>
            <tbody>
            <tr>
                <th><?php echo $object[0]->description ?></th>
                <th><img src="data:image/jpeg;base64,<?php echo base64_encode($object[0]->image) ?>" height="150px"
                         width="150px"/></th>
                <th><?php echo $object[0]->price ?></th>
                <th><a href="cart?id=<?php echo $object[0]->id_product ?>"> Купить</a></th>
            </tr>
            </tbody>
            </thead>
        </table>
    </div>

</div> <!-- /container -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</body>
</html>
