<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">


    <title><?php echo $cart[0]->name ?></title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar.css" rel="stylesheet">

</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../">Главная</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/category">Категории</a></li>
                    <li><a>Покупка</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a>Меню администратора:</a></li>
                    <li><a href="/admCategory">Категории</a></li>
                    <li><a href="/admProduct">Товары</a></li>
                    <li><a href="/admOrders">Заказы</a></li>
                </ul>
                </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h1><?php echo $cart[0]->name ?></h1>

        <form action="/succes" method="post">
            <p>Ваше имя: <input type="text" name="firstName"/></p>
            <p>Ваша фамилия: <input type="text" name="lastName"/></p>
            <p>Почта: <input type="email" name="mail"/></p>
            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
            <p><input type="submit"/></p>
        </form>
    </div>

</div> <!-- /container -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</body>
</html>
