<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Добавление новых товаров</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar.css" rel="stylesheet">

</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../">Главная</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/category">Категории</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a>Меню администратора:</a></li>
                    <li><a href="/admCategory">Категории</a></li>
                    <li class="active"><a href="/admProduct">Товары</a></li>
                    <li><a href="/admOrders">Заказы</a></li>
                </ul>
                </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h1>Добавление новых товаров</h1>
        <table class="table">
            <thead>
            <tr>
                <th>Категория</th>
                <th>Название товара</th>
                <th>Описание</th>
                <th>Изображение</th>
                <th>Цена</th>
            </tr>
            </thead>
            <tr>
                <form action="/admProduct" method="post" enctype=multipart/form-data>
                    <th><select name="category">
                            <?php foreach ($categories as $category): ?>
                                <option value="<?php echo $category->id_category ?>">
                                    <? echo $category->name ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </th>
                    <th><input type="text" name="name"/></th>
                    <th><input type="text" name="description"/></th>
                    <th><input type="file" name="image"/></th>
                    <th><input type="text" name="price"/></th>
                    <th>
                        <button type="submit"/>
                        Загрузить</button></th>
                </form>
            </tr>

            <tbody>
            <?php foreach ($products as $product): ?>
                <tr>
                    <th></th>
                    <th><?php echo $product->name ?></th>
                    <th><?php echo $product->description ?></th>
                    <th><img src="data:image/jpeg;base64,<?php echo base64_encode($product->image) ?>" height="150px"
                             width="150px"/></th>
                    <th><?php echo $product->price ?></th>
                    <th><a href="admProduct?delete=<?php echo $product->id_product ?>">Удалить</a></th>
                </tr>
            <?php endforeach; ?>
            </tbody>

        </table>
    </div>

</div> <!-- /container -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</body>
</html>
