<?php
namespace Controllers;

use Model\cart_model;

class succes_controller extends abstractController
{

    protected function actionIndex()
    {
        if (isset($_POST['id']) && ($_POST['id'] > 0) &&
            (isset($_POST['firstName']) && !empty($_POST['firstName'])) &&
            (isset($_POST['lastName']) && !empty($_POST['lastName'])) &&
            isset($_POST['mail']) && !empty($_POST['mail'])
        ) {
            $data = array('', trim($_POST['id']), trim($_POST['firstName']), trim($_POST['lastName']),
                date("Y,m,d"), trim($_POST['mail']));
            cart_model::acceptPurchase($data);
            echo 'Ваш заказ принят';
        } else
            echo 'Произошла ошибка, попробуйте снова!';

        $this->view->categories = \Model\category_model::showCategory();
        $this->view->display(__DIR__ . '\..\View\category_view.php');
    }
}