<?php
namespace Controllers;

class cart_controller extends abstractController
{

    protected function actionIndex()
    {
        $params = [];
        if (isset($_GET['id'])) {
            $params[':id'] = $_GET['id'];
            $this->view->cart = \Model\object_model::showObject($params);
        } else $this->view->cart = $params;
        $this->view->display(__DIR__ . '\..\View\cart_view.php');
    }
}