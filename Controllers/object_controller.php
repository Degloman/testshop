<?php
namespace Controllers;

class object_controller extends abstractController
{

    protected function actionIndex()
    {
        $params = [];
        if (isset($_GET['id'])) {
            $params[':id'] = $_GET['id'];
            $this->view->object = \Model\object_model::showObject($params);
        } else $this->view->object = $params;

        $this->view->display(__DIR__ . '\..\View\object_view.php');
    }
}