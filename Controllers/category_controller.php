<?php
namespace Controllers;

class category_controller extends abstractController
{

    protected function actionIndex()
    {
        $this->view->title = 'Мой крутой сайт!';
        $this->view->categories = \Model\category_model::showCategory();
        $this->view->display(__DIR__ . '\..\View\category_view.php');
    }
}