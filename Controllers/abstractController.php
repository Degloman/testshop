<?php
namespace Controllers;

use View\View;

abstract class abstractController
{
    protected $view;

    public function __construct()
    {
        $this->view = new View();
    }

    public function action($action)
    {
        $methodName = 'action' . $action;
        $this->beforeAction();
        return $this->$methodName();
    }

    protected function beforeAction()
    {
        //if something more need
    }

    protected function actionIndex()
    {
        //some action
    }

    //    protected function actionOne()
//    {
//        $id = (int)$_GET['id'];
//        $this->view->article = \App\Models\News::findById($id);
//        $this->view->display(__DIR__ . '/../templates/one.php');
//    }
}