<?php
namespace Controllers;

class product_controller extends abstractController
{

    protected function actionIndex()
    {
        $params = [];
        if (isset($_GET['id'])) {
            $params[':id'] = $_GET['id'];
            $this->view->products = \Model\product_model::showProducts($params);
        } else $this->view->products = $params; //заглушка

        $this->view->display(__DIR__ . '\..\View\product_view.php');
    }
}