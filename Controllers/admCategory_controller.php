<?php
namespace Controllers;

use Model\category_model;


class admCategory_controller extends abstractController
{
    protected function actionIndex()
    {
        if (isset($_GET['delete']) && !empty($_GET['delete'])) {
            category_model::deleteCategory($_GET['delete']);
        }
        
        if (isset($_POST['category']) && !empty($_POST['category'])) {
            category_model::insertCategory(array('', $_POST['category']));
        }

        $this->view->categories = \Model\category_model::showCategory();
        $this->view->display(__DIR__ . '\..\View\admCategory_view.php');
    }
}