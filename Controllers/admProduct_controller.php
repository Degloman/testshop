<?php
namespace Controllers;

use Model\product_model;

class admProduct_controller extends abstractController
{

    protected function actionIndex()
    {
        if ((isset($_POST['name']) && !empty($_POST['name'])) &&
            (isset($_POST['description']) && !empty($_POST['description'])) &&
            isset($_FILES['image']) && !empty($_FILES['image']) &&
            isset($_POST['price']) && !empty($_POST['price'])
        ) {
            //сохраняем файл
            $handler = fopen($_FILES['image']['tmp_name'], 'r');
            $file = fread($handler, filesize($_FILES['image']['tmp_name']));
            //отправляем данные в базу
            $data = ['', $_POST['category'], $_POST['name'], $_POST['description'], $_POST['price'], $file];
            product_model::insertProduct($data);
        }

        if (isset($_GET['delete']) && !empty($_GET['delete'])) {
            product_model::deleteByID($_GET['delete']);
        }
        $this->view->categories = \Model\category_model::showCategory();
        $this->view->products = \Model\product_model::showAllProducts();
        $this->view->display(__DIR__ . '\..\View\admProduct_view.php');
    }
}