<?php
namespace Controllers;

use Model\orders_model;

class admOrders_controller extends abstractController
{
    protected function actionIndex()
    {
        if (isset($_GET['delete']) && !empty($_GET['delete'])) {
            orders_model::deleteOrder($_GET['delete']);
        }
        $this->view->orders = orders_model::showOrders();
        $this->view->display(__DIR__ . '\..\View\admOrders_view.php');
    }
}